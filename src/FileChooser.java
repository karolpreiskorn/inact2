
/*
 * Copyright (c) 2016 TMPL All rights reserved.
 *
 * @todo dodanie popup z wyborem 
 * Impoert danych do rekonstrukcji
 * Otwarcie Excel
 * Odczyt danych
 * Zbudowanie skryptu lub wywolanie procedury bazodanowej
 * commit
 * 
 * push to bitbucket
 * 
 */

import java.awt.BorderLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/*
 * FileChooserDemo.java uses these files:
 *   images/Open16.gif
 *   images/Save16.gif
 */
public class FileChooser extends JPanel implements ActionListener {
  /**
   * 
   */
  private static final long   serialVersionUID = 1L;
  static private final String newline          = "\n";
  JButton                     openButton, saveButton;
  JTextArea                   log;
  JFileChooser                fc;
  HSSFWorkbook                wb;
                              
  public FileChooser() {
    super(new BorderLayout());
    
    // Create the log first, because the action listeners
    // need to refer to it.
    log = new JTextArea(10, 40);
    log.setMargin(new Insets(5, 5, 5, 5));
    log.setEditable(false);
    JScrollPane logScrollPane = new JScrollPane(log);
    
    // Create a file chooser
    fc = new JFileChooser(".");
    
    // Uncomment one of the following lines to try a different
    // file selection mode. The first allows just directories
    // to be selected (and, at least in the Java look and feel,
    // shown). The second allows both files and directories
    // to be selected. If you leave these lines commented out,
    // then the default mode (FILES_ONLY) will be used.
    //
    // fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
    
    FileNameExtensionFilter filter = new FileNameExtensionFilter("Excel files", "xls", "xlsx");
    fc.setFileFilter(filter);
    
    // Create the open button. We use the image from the JLF
    // Graphics Repository (but we extracted it from the jar).
    openButton = new JButton("Open a File...", createImageIcon("images/Open16.gif"));
    openButton.addActionListener(this);
    
    // Create the save button. We use the image from the JLF
    // Graphics Repository (but we extracted it from the jar).
    saveButton = new JButton("Save a File...", createImageIcon("images/Save16.gif"));
    saveButton.addActionListener(this);
    
    // For layout purposes, put the buttons in a separate panel
    JPanel buttonPanel = new JPanel(); // use FlowLayout
    buttonPanel.add(openButton);
    buttonPanel.add(saveButton);
    
    // Add the buttons and the log to this panel.
    add(buttonPanel, BorderLayout.PAGE_START);
    add(logScrollPane, BorderLayout.CENTER);
  }
  
  @Override
  public void actionPerformed(ActionEvent e) {
    
    // Handle open button action.
    if (e.getSource() == openButton) {
      int returnVal = fc.showOpenDialog(FileChooser.this);
      
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        File file = fc.getSelectedFile();
        // This is where a real application would open the file.
        log.append("Opening: " + file.getAbsolutePath() + "." + newline);
        // Call read script
        
        try {
          wb = HSSFReadWrite.readFile(file.getAbsolutePath());
        } catch (IOException e1) {
          // TODO Auto-generated catch block
          e1.printStackTrace();
        }
        
        /**
         * file to write information
         * 
         * @todo the same read file
         */
        FileOutputStream stream = null;
        try {
          stream = new FileOutputStream(file.getAbsolutePath());
        } catch (FileNotFoundException e2) {
          // TODO Auto-generated catch block
          e2.printStackTrace();
        }
        
        System.out.println("Data dump:\n");
        
        for (int k = 0; k < wb.getNumberOfSheets(); k++) {
          HSSFSheet sheet = wb.getSheetAt(k);
          int rows = sheet.getPhysicalNumberOfRows();
          System.out.println("Sheet " + k + " \"" + wb.getSheetName(k) + "\" has " + rows + " row(s).");
          for (int r = 0; r < rows; r++) {
            HSSFRow row = sheet.getRow(r);
            if (row == null) {
              continue;
            }
            
            int cells = row.getPhysicalNumberOfCells();
            
            System.out.println("\nROW " + row.getRowNum() + " has " + cells + " cell(s).");
            for (int c = 0; c < cells; c++) {
              HSSFCell cell = row.getCell(c);
              String value = null;
              
              switch (cell.getCellType()) {
                
                case HSSFCell.CELL_TYPE_FORMULA:
                  value = "FORMULA value=" + cell.getCellFormula();
                  break;
                  
                case HSSFCell.CELL_TYPE_NUMERIC:
                  value = "NUMERIC value=" + cell.getNumericCellValue();
                  break;
                  
                case HSSFCell.CELL_TYPE_STRING:
                  value = "STRING value=" + cell.getStringCellValue();
                  break;
                  
                default:
              }
              System.out.println("CELL col=" + cell.getColumnIndex() + " VALUE=" + value);
              
            }
            
            /**
             * @todo add confirmation ie. write to the same excel file (rw)
             */
            HSSFCell cell = row.getCell(row.getRowNum());
            cell = row.getCell(row.getRowNum());
            cell.setCellValue("MODIFIED CELL!!!!!");
            
          }
          try {
            wb.write(stream);
            stream.close();
            wb.close();
          } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
          }
          
        }
        try {
          wb.close();
        } catch (IOException e1) {
          // TODO Auto-generated catch block
          e1.printStackTrace();
        }
      } else {
        log.append("Open command cancelled by user." + newline);
      }
      log.setCaretPosition(log.getDocument().getLength());
      
      // Handle save button action.
    } else if (e.getSource() == saveButton) {
      int returnVal = fc.showSaveDialog(FileChooser.this);
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        File file = fc.getSelectedFile();
        // This is where a real application would save the file.
        log.append("Saving: " + file.getName() + "." + newline);
      } else {
        log.append("Save command cancelled by user." + newline);
      }
      log.setCaretPosition(log.getDocument().getLength());
    }
  }
  
  /** Returns an ImageIcon, or null if the path was invalid. */
  protected static ImageIcon createImageIcon(String path) {
    java.net.URL imgURL = FileChooser.class.getResource(path);
    if (imgURL != null) {
      return new ImageIcon(imgURL);
    } else {
      System.err.println("error: Couldn't find file: " + path);
      return null;
    }
  }
  
  /**
   * Create the GUI and show it. For thread safety, this method should be invoked from the event dispatch thread.
   */
  private static void createAndShowGUI() {
    // Create and set up the window.
    JFrame frame = new JFrame("FileChooser - Start reconstruction");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
    // Add content to the window.
    frame.add(new FileChooser());
    
    // Display the window.
    frame.pack();
    
    // center to the screen
    frame.setLocationRelativeTo(null);
    frame.setVisible(true);
  }
  
  public static void main(String[] args) {
    // Schedule a job for the event dispatch thread: creating and showing
    // this application's GUI.
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        // Turn off metal's use of bold fonts
        UIManager.put("swing.boldMetal", Boolean.FALSE);
        createAndShowGUI();
      }
    });
  }
}
